<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web Routes for your application. These
| Routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    phpinfo();
    return view('welcome');
});
Route::get('admin/login', 'AuthController@getLogin')->name('system.getLogin');
Route::post('admin/login', 'AuthController@postLogin')->name('system.postLogin');
Route::get('admin/forgot', 'AuthController@getForgot')->name('system.getForgot');
Route::post('admin/forgot', 'AuthController@postForgot')->name('system.postForgot');
Route::get('admin/register', 'AuthController@getRegister')->name('system.getRegister');
Route::post('admin/register', 'AuthController@postRegister')->name('system.postRegister');
Route::get('logout', 'AuthController@getLogout')->name('getLogout');

Route::get('test', 'TestController@getTest');

Route::group(['prefix' => 'system'], function () {

    Route::get('dashboard', 'DashboardController@getIndex')->name('system.dashboard');

    Route::group(['prefix' => 'product'], function () {
        Route::get('product', 'ProductController@getProduct')->name('system.Product');
        Route::post('add', 'ProductController@postAddProduct')->name('system.postAddProduct');
        Route::post('edit', 'ProductController@postEditProduct')->name('system.postEditProduct');
        Route::post('delete', 'ProductController@postDeleteProduct')->name('system.postDeleteProduct');

        Route::get('typeproduct', 'ProductController@getTypeProduct')->name('system.Typeproduct');
        Route::post('addTypeProduct', 'ProductController@postAddTypeProduct')->name('system.postAddTypeProduct');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('index', 'UserController@getIndex')->name('system.ManagerUser');
        Route::get('profile', 'UserController@getProfile')->name('system.Profile');
        Route::post('updateUserStatus', 'UserController@updateUserStatus')->name('system.updateUserStatus');
        Route::post('postChangePassword', 'UserController@postChangePassword')->name('system.postChangePassword');
    });

    Route::group(['prefix' => 'orderlist'], function () {
        Route::get('index', 'OrderListController@getIndex')->name('system.OrderList');
    });
});