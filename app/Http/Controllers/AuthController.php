<?php

namespace App\Http\Controllers;

use App\Model\UserModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Hash;
use Session;

class AuthController extends Controller
{
    //logout function
    public function getLogout(){
	    if(session('userTemp') && session('routeMember')){
            $sessionOld = session('userTemp');
            // bỏ session cũ
            Session::forget('user');
            Session::forget('userTemp');

            // tạo session mới
            Session::put('user', $sessionOld);
            $routeTemp  = Session('routeMember');
            Session::forget('routeMember');
            return redirect()->route($routeTemp)->with(['flash_level'=>'error', 'flash_message'=>'Logout Success']);
        }

        Session::forget('user');
        return redirect()->route('system.getLogin');
    }

    //login function
    public function getLogin()
    {
        return view('System.Auth.Login');
    }
    public function postLogin(Request $request)
    {
        // dd($request);
        $this->validate(
            $request,
            [
                'email' => 'required',
                'password' => 'required|min:3|max:32'
            ],
            [
                'email.required' => 'You did not enter an email',
                'password.required' => 'You did not enter a password',
                'password.min' => 'Password cannot be less than 3 characters',
                'password.max' => 'Password cannot be larger than 32 characters',
            ]
        );
        $userCheck = UserModel::where('user_email', $request->email)->first();
        if ($userCheck) {

            //check block user

            if ($userCheck->user_status && $userCheck->user_status == 0) {
                return redirect()->route('system.getLogin');
            }
            if (!$userCheck->user_level || $userCheck->user_level !== 1) {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Permission denied']);;
            }
            //check password
            if (Hash::check($request->password, $userCheck->user_password)) {

                Session::put('user', $userCheck);

                return redirect()->route('system.dashboard')->with(['flash_level' => 'success', 'flash_message' => 'Login Success!']);
            } else {

                return redirect()->route('system.getLogin')->with(['flash_level' => 'error', 'flash_message' => 'Incorrect username or password']);
            }
        } else {
            return redirect()->route('system.getLogin')->with(['flash_level' => 'error', 'flash_message' => 'User is not exist']);
        }
        // dd(['user_email' => $request->email, 'user_password' => $request->password]);
        // if (Auth::attempt(['user_email' => $request->email, 'user_password' => $request->password])) {
        //     return redirect()->route('system.dashboard');
        // } else {
        //     return  redirect('admin/login')->with('messenger', 'Login unsuccessful');
        // }
        return redirect()->route('system.getLogin');
    }
    public function getForgot()
    {
        return view('System.Auth.Forgot');
    }
    public function postForgot()
    {
        return view('System.Auth.Forgot');
    }
    public function getRegister()
    {
        $register = UserModel::all();
        return view('System.Auth.Register', compact('register'));
    }
    public function postRegister(Request $request)
    {
        $this->validate(
            $request,
            [
                'email' => 'required',
                'password' => 'required|min:3|max:32',
                'username' => 'required'

            ],
            [
                'email.required' => 'You did not enter an email',
                'password.required' => 'You did not enter a password',
                'password.min' => 'Password cannot be less than 3 characters',
                'password.max' => 'Password cannot be larger than 32 characters',
                'username.required' => 'You did not enter an name'
            ]
        );
        $check = UserModel::where('user_email', $request->email)->first();
        $user_id = $this->RandonIDUser();
        if ($check) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Email already exists']);
        } else {
            $register = new UserModel;
            $register->user_id = $user_id;
            $register->user_email = $request->email;
            $register->user_name = $request->username;
            $register->user_password = bcrypt($request->password);
            $register->save();
        }
        return redirect()->route('system.getLogin')->with(['flash_level' => 'success', 'flash_message' => 'You have successfully added']);
    }
    public static function RandonIDUser(){
	    
	    $id = rand(100000, 999999);
        $user = UserModel::where('user_ID',$id)->first();
        if(!$user){
            return $id;
        }else{
            return $this->RandonIDUser();
        }
	}
}