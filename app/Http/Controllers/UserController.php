<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\UserModel;
use Hash;


class UserController extends Controller
{
    //
    public function getIndex(){
/*
	    $newUser = new UserModel();
	    $newUser->user_id = 666666;
	    $newUser->user_name = 'quocnguen0993';
	    $newUser->user_wallet = null;
	    $newUser->user_balance = 0;
	    $newUser->user_email = 'quocnguen0993@gmail.com';
	    $newUser->user_password = bcrypt('123123');
	    $newUser->user_phone = '0971838335';
	    $newUser->user_deposit = 0;
	    $newUser->user_withdraw = 0;
	    $newUser->user_parent = 999999;
	    $newUser->user_tree = '999999,666666';
	    $newUser->user_status = 1;
	    $newUser->user_level = 1;
	    $newUser->save();
*/
		$UserList = UserModel::get();

        return view('System.User.Index', compact('UserList'));
    }
    public function getProfile(){
        return view('System.User.Profile');
	}
	public function updateUserStatus(Request $request)
    {
		$user = UserModel::where('user_id', $request->user_id)->first();
		$user_status = $user->user_status;
		try {
			//code...
			if((int)$user_status!=0){
				UserModel::where('user_id', $request->user_id)->update(['user_status' => '0']);
				// return redirect()->back()->with(['flash_level' => 'success', 'Your Block user completed']);
				
				return response()->json(json_encode(array('status'=>true, 'message'=>'Your Block user completed !')), 200);
				
			}else{
				UserModel::where('user_id', $request->user_id)->update(['user_status' => '1']);
				// return redirect()->back()->with(['flash_level' => 'success', 'Your UnBlock user completed']);
				return response()->json(json_encode(array('status'=>true, 'message'=>'Your UnBlock user completed !')), 200);
			}
			
			
		} catch (Thow $th) {
			throw $th;
			// return redirect()->back()->with(['flash_level' => 'error', 'Please contact to admin about this issue']);
			return response()->json(json_encode(array('status'=>false, 'message'=>'Please contact to admin about this issue !')), 200);
		}
		

		
	}
	public function postChangePassword(Request $request)
    {
	
		$password = '123456';	
			
		UserModel::where('user_id', $request->user_id)->update('user_password',$password);
		
		
		return response()->json(json_encode(array('status'=>true, 'message'=>'Your change password user completed !')), 200);
		
    }
	
    
    
}