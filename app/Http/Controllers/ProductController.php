<?php

namespace App\Http\Controllers;

use App\Model\CategoryModel;
use App\Model\ProductModel;
use Illuminate\Http\Request;
use Storage;

class ProductController extends Controller
{
    //
    public function getProduct()
    {
        $product = ProductModel::all();
        // dd( $product);
        return view('System.Product.Product', compact('product'));
    }
    public function postAddProduct(Request $request)
    {
        // dd($request);
        $this->validate(
            $request,
            [
                'productName' => 'required|min:3',
                'productQuantity' => 'required',
                'productPrice' => 'required',
                'productPriceMin' => 'required',
                'productPriceMax' => 'required',
                'productVote' => 'required',
                'productMinPeople' => 'required',
                'productTitle' => 'required',
                'productType' => 'required',
                'productDescription' => 'required',
                'productImages' => 'required'
            ],
            [
                'productName.required' => 'You have not entered a product name',
                'productName.min' => 'Password cannot be less than 3 characters',
                'productQuantity.required' => 'You have not entered a product quantity',
                'productPrice.required' => 'You have not entered a product price',
                'productPriceMin.required' => 'You have not entered a product price min',
                'productPriceMax.required' => 'You have not entered a product price max',
                'productVote.required' => 'You have not entered a product vote',
                'productMinPeople.required' => 'You have not entered a product people min',
                'productTitle.required' => 'You have not entered a product title',
                'productType.required' => 'You have not entered a product type',
                'productDescription.required' => 'You have not entered a product description',
                'productImages.required' => 'You have not entered a product images',
            ]
        );
        $product = new ProductModel;
        $product->product_name = $request->productName;
        $product->product_quantity = $request->productQuantity;
        $product->product_price = $request->productPrice;
        $product->product_priceMin = $request->productPriceMin;
        $product->product_priceMax = $request->productPriceMax;
        $product->product_vote = $request->productVote;
        //dd($product->product_vote);
        $product->product_minimumPeople = $request->productMinPeople;
        $product->product_title = $request->productTitle;
        $product->product_categoryId = $request->productType;
        $product->product_description = $request->productDescription;
        //$product->product_images = $request->productImages;
        if ($request->hasFile('productImages')) {
            $user = session('user');
            $file = $request->file('productImages');
            $duoi = $file->getClientOriginalExtension();
            if ($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg') {
                return redirect()->back()->with(['flash_level' => 'error', 'You can only choose files ending with jpg, png, jpeg']);
            }

            //get file extension
            $productImageExtension = $request->file('productImages')->getClientOriginalExtension();
            //set folder and file name
            $randomNumber = uniqid();
            $productImageStore = "file/" . $user->user_id . "/profile/product_image_" . $randomNumber . "." . $productImageExtension;
            //send to Image server
            $productImageStatus = Storage::disk('ftp')->put($productImageStore, fopen($request->file('productImages'), 'r+'));
            //save name image
            if ($productImageStatus) {
                $product->product_images = $productImageStore;
                //save product
                $product->save();
                return redirect()->back()->with(['flash_level' => 'success', 'Add product success!']);
            }
            return redirect()->back()->with(['flash_level' => 'error', 'please contact admin!']);
        } else {
            return redirect()->back()->with(['flash_level' => 'error', 'You can choose image product !']);
        }

        return redirect()->back()->with(['flash_level' => 'error', 'Add product fail']);
    }
    public function postEditProduct(Request $request)
    {
        //$request = '5ef5a67218c02b5a6d3610f2';
        $product = ProductModel::where('_id', $request->product_id)->first();
        //dd($product->product_name);

        $this->validate(
            $request,
            [
                'productName' => 'required|min:3',
                // 'productQuantity' => 'required',
                // 'productPrice' => 'required',
                // 'productPriceMin' => 'required',
                // 'productPriceMax' => 'required',
                // 'productVote' => 'required',
                // 'productMinPeople' => 'required',
                // 'productTitle' => 'required',
                // 'productType' => 'required',
                // 'productDescription' => 'required',
                // 'productImages' => 'required'
            ],
            [
                'productName.required' => 'You have not entered a product name',
                'productName.min' => 'Password cannot be less than 3 characters',
                // 'productQuantity.required' => 'You have not entered a product quantity',
                // 'productPrice.required' => 'You have not entered a product price',
                // 'productPriceMin.required' => 'You have not entered a product price min',
                // 'productPriceMax.required' => 'You have not entered a product price max',
                // 'productVote.required' => 'You have not entered a product vote',
                // 'productMinPeople.required' => 'You have not entered a product people min',
                // 'productTitle.required' => 'You have not entered a product title',
                // 'productType.required' => 'You have not entered a product type',
                // 'productDescription.required' => 'You have not entered a product description',
                // 'productImages.required' => 'You have not entered a product images',
            ]
        );
        //$product = ProductModel::find($id);
        // $test = ProductModel::find('nam');
        // dd($test);

        $product->product_name = $request->productName;
        $product->product_quantity = $request->productQuantity;
        $product->product_price = $request->productPrice;
        $product->product_priceMin = $request->productPriceMin;
        $product->product_priceMax = $request->productPriceMax;
        $product->product_vote = $request->productVote;
        //dd($product->product_vote);
        $product->product_minimumPeople = $request->productMinPeople;
        $product->product_title = $request->productTitle;
        $product->product_categoryId = $request->productType;
        $product->product_description = $request->productDescription;
        //$product->product_images = $request->productImages;
        if ($request->hasFile('productImages')) {
            $user = session('user');
            $file = $request->file('productImages');
            $duoi = $file->getClientOriginalExtension();
            if ($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg') {
                return redirect()->back()->with(['flash_level' => 'error', 'You can only choose files ending with jpg, png, jpeg']);
            }

            //get file extension
            $productImageExtension = $request->file('productImages')->getClientOriginalExtension();
            //set folder and file name
            $randomNumber = uniqid();
            $productImageStore = "file/" . $user->user_id . "/profile/product_image_" . $randomNumber . "." . $productImageExtension;
            //send to Image server
            $productImageStatus = Storage::disk('ftp')->put($productImageStore, fopen($request->file('productImages'), 'r+'));
            //save name image
            if ($productImageStatus) {
                $product->product_images = $productImageStore;
                //save product
                $product->save();
                return redirect()->back()->with(['flash_level' => 'success', 'Add product success!']);
            }
            return redirect()->back()->with(['flash_level' => 'error', 'please contact admin!']);
        } else {
            return redirect()->back()->with(['flash_level' => 'error', 'You can choose image product !']);
        }

        return redirect()->back()->with(['flash_level' => 'error', 'Add product fail']);
    }
    public function postDeleteProduct(Request $request)
    {

        // $product = ProductModel::find();
        $product = ProductModel::where('_id', $request->product_id)->first();
        // if(!$product){
        //     return response()->json(json_encode(array('status'=>false, 'message'=>'delete product failed!')), 200);
        // }
        // dd($product);
        // $product->delete();
        // return response()->json(json_encode(array('status'=>true, 'message'=>'delete product success!')), 200);
    }
    //
    public function getTypeProduct()
    {
        $typeproduct = CategoryModel::all();
        return view('System.Product.TypeProduct', compact('typeproduct'));
    }
    public function postAddTypeProduct(Request $request)
    {
        // dd($request);
        $this->validate(
            $request,
            [
                'productName' => 'required|min:3',
                'productQuantity' => 'required',
                'productPrice' => 'required',
                'productType' => 'required',
                'productDescription' => 'required',
                'productImages' => 'required'
            ],
            [
                'productName.required' => 'You have not entered a product name',
                'productName.min' => 'Password cannot be less than 3 characters',
                'productQuantity.required' => 'You have not entered a product quantity',
                'productPrice.required' => 'You have not entered a product price',
                'productType.required' => 'You have not entered a product type',
                'productDescription.required' => 'You have not entered a product description',
                'productImages.required' => 'You have not entered a product images',
            ]
        );
        $category = new CategoryModel;
        $category->product_name = $request->productName;
        $category->product_quantity = $request->productQuantity;
        $category->product_price = $request->productPrice;
        $category->product_categoryId = $request->productType;
        $category->product_description = $request->productDescription;
        //$product->product_images = $request->productImages;
        if ($request->hasFile('productImages')) {
            $user = session('user');
            $file = $request->file('productImages');
            $duoi = $file->getClientOriginalExtension();
            if ($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg') {
                return redirect()->back()->with(['flash_level' => 'error', 'You can only choose files ending with jpg, png, jpeg']);
            }


            //get file extension
            $productImageExtension = $request->file('productImages')->getClientOriginalExtension();
            //set folder and file name
            $randomNumber = uniqid();
            $productImageStore = "file/" . $user->user_id . "/profile/product_image_" . $randomNumber . "." . $productImageExtension;
            //send to Image server
            $productImageStatus = Storage::disk('ftp')->put($productImageStore, fopen($request->file('productImages'), 'r+'));
            //save name image
            if ($productImageStatus) {
                $product->product_images = $productImageStore;
                //save product
                $product->save();
                return redirect()->back()->with(['flash_level' => 'success', 'Add product success!']);
            }
            return redirect()->back()->with(['flash_level' => 'error', 'please contact admin!']);
        } else {
            return redirect()->back()->with(['flash_level' => 'error', 'You can choose image product !']);
        }

        return redirect()->back()->with(['flash_level' => 'error', 'Add product fail']);
    }
    // public static function RandonIDProduct()
    // {

    //     $id = rand(100000, 999999);
    //     $user = ProductModel::where('product_id', $id)->first();
    //     if (!$user) {
    //         return $id;
    //     } else {
    //         return $this->RandonIDUser();
    //     }
    // }
}