<?php

namespace App\Model;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Auth\User as Authenticatable;

class UserModel extends Authenticatable
{
    use Notifiable;
    protected $connection = 'mongodb';
    protected $collection = 'users';
    protected $fillable = [
        '_id',
        'user_id',
        'user_name',
        'user_wallet',
        'user_balance',
        'user_email',
        'user_password',
        'user_phone',
        'user_deposit',
        'user_withdraw',
        'user_parent',
        'user_tree',
        'user_status',
        'user_level'
    ];

    public $timestamps = true;
}