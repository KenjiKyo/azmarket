<?php

namespace App\Model;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


use DB;

class LogModel extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'logs';
    protected $fillable = [
        '_id', 'log_user', 'log_action', 'log_logId', 'log_amount', 'log_hash', 'log_confirm', 'log_datetime'
    ];

    public $timestamps = true;
}