<?php

namespace App\Model;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


use DB;

class OrderModel extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'orders';
    protected $fillable = [
        '_id', 'order_name', 'order_status', 'order_datetime', 'order_userId', 'order_productId'
    ];

    public $timestamps = true;
}