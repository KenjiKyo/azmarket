<?php

namespace App\Model;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


use DB;

class CurencyModel extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'curencies';
    protected $fillable = [
        '_id', 'curency_name', 'curency_symbol', 'curency_status'
    ];

    public $timestamps = true;
}