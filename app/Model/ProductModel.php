<?php

namespace App\Model;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


use DB;

class ProductModel extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'products';
    protected $fillable = [
        '_id', 'product_id', 'product_name', 'product_price', 'product_priceMax', 'product_priceMin', 'product_vote', 'product_quantity', 'product_minimumPeople', 'product_status', 'product_description', 'product_images', 'product_content', 'product_title', 'product_userId', 'product_categoryId'
    ];

    public $timestamps = true;
}