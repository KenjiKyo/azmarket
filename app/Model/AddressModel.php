<?php

namespace App\Model;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


use DB;

class AddressModel extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'address';
    protected $fillable = [
        '_id', 'address_user', 'address_curency', 'address_addressId', 'address_status', 'address_datetime'
    ];

    public $timestamps = true;
}