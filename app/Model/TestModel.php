<?php

namespace App\Model;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Carbon\Carbon;


use DB;

class TestModel extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'actions';
    protected $fillable = [
        '_id', 'action_name'
    ];

    public $timestamps = true;
}