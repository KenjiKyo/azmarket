<?php

namespace App\Model;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


use DB;

class CategoryModel extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'categories';
    protected $fillable = [
        '_id', 'category_name', 'category_description', 'category_icon', 'category_title', 'category_images', 'category_status', 'category_searchDescription', 'category_categoryId'
    ];

    public $timestamps = true;
}