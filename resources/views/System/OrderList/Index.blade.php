@extends('System.Layouts.Master')
@section('title')
DashBoard
@endsection
@section('css')
@endsection
@section('content-header')
<div class="container-fluid">
  <!-- Page-Title -->
  <div class="row">
    <div class="col-sm-12">
      <div class="page-title-box">
      
        <h4 class="page-title">
          <i class="dripicons-blog"></i> Product Management
        </h4>
      </div>
    </div>
  </div>
  <!-- end page title end breadcrumb -->
</div>
@endsection
@section('content-body')
<div class="container-fluid">
  <div class="row">
    
    <div class="col-lg-8 mx-auto">
      <div class="card m-b-20">
          <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                 
                 
                </div>
                <div class="col-md-12">
                  <h4 class="mt-0 header-title">Search</h4>
                  <form action="#" id="form-suser">
                    <div class="row">
                      <div class="col-md-12 col-lg-6">
                        <div class="form-group">
                            <label>Email Order</label>
                            <input class="form-control" type="text" value="" id="example-text-input" placeholder="User Email">
                        </div>
                    </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group">
                                <label>User Order</label>
                                <input class="form-control" type="text" value="" id="example-text-input" placeholder="User ID">
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                          <div class="form-group">
                              <label>Oder ID</label>
                              <input class="form-control" type="text" value="" id="example-text-input" placeholder="User ID">
                          </div>
                      </div>
                    
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group">
                                <label>Order Status</label>
                                <select class="form-control">
                                    <option>Select</option>
                                    <option>Large select</option>
                                    <option>Small select</option>
                                </select>
                            </div>
                        </div>
                      
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group">
                                <label>Order Date From</label>
                                <div>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                        </div>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group">
                                <label>Order Date To</label>
                                <div>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose-1">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                        </div>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary waves-effect  waves-light">Search</button>
                                <button type="button" onclick="resetForm()" class="btn btn-warning  waves-effect waves-light">Cancel</button>
                            </div>
                        </div>
                      </div>
                    </form>
                </div>
              </div>

         
          </div>
      </div>

    </div>
    <div class="col-12">
      <div class="card m-b-20">
        <div class="card-body">
          <table id="datatable-buttons-4" class="table table-striped table-bordered dt-responsive " style="
              border-collapse: collapse;
              border-spacing: 0;
              width: 100%;
            ">
            <thead>
              <tr>

                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th>Price Max</th>
                <th>Price Min</th>
                <th>Vote</th>
                <th>Image</th>
                <th>Category</th>
                <th>Description</th>
                <th>Content</th>
                <th>Title</th>
                <th>Date Create</th>
                <th>Date Update</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- end col -->
  </div>
  <!-- end row -->
</div>
<div class="modal fade bs-example-modal-center" id="modalEditCreate"tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title mt-0"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
              <form action="">
                <div class="row">
                  <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                        <label>User Email</label>
                        <input class="form-control" type="text" value="" id="example-text-input" placeholder="User ID">
                    </div>
                  </div>
                  <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                        <label>User ID</label>
                        <input class="form-control" type="text" value="" id="example-text-input" placeholder="User ID">
                    </div>
                  </div>
                  <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                        <label>ID Order</label>
                        <input class="form-control" type="text" value="" id="example-text-input" placeholder="User ID">
                    </div>
                  </div>
                  <div class="col-md-12 col-lg-6">
                      <div class="form-group">
                          <label>Product Order</label>
                          <input class="form-control" type="number" value="" id="example-text-input" placeholder="User ID">
                      </div>
                  </div>
                  <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                        <label>Product QTY Order</label> 
                        <input class="form-control" type="number" value="" id="example-text-input" placeholder="User ID">
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                  <div class="form-group">
                      <label>Price Total</label> 
                      <input class="form-control" type="number" value="" id="example-text-input" placeholder="User ID">
                  </div>
              </div>
              <div class="col-md-12 col-lg-12">
                <div class="form-group">
                    <label>Comment</label> 
                    <textarea id="textarea" class="form-control" maxlength="225" rows="3" placeholder="This textarea has a limit of 225 chars."></textarea>
                  </div>
              </div>
                  
                <div class="col-md-12">
                  <div class="form-group text-right">
                      <button class="btn btn-success waves-light waves">Success</button>
                      <button class="btn btn-warning waves-light waves">Cancel Bill</button>
                      <button class="btn btn-danger waves-light waves" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </form>
          </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

@endsection
@section('scripts')
<!-- Required datatable js -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/buttons.colVis.min.js"></script>
<!-- Responsive examples -->
<script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables/responsive.bootstrap4.min.js"></script>


<!-- Plugins js -->
<script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

<!-- Plugins Init js -->
<script src="assets/pages/form-advanced.js"></script>
<!-- Datatable init js -->
<script src="assets/pages/datatables.init.js?v=1"></script>

<!-- App js -->
<script src="assets/js/app.js"></script>
<script>
  function resetForm(){
        document.getElementById("form-suser").reset();
    }
    $('#modalEditCreate').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) 
      var title = button.data('title') 
    
      var modal = $(this)
      modal.find('.modal-title').text(title)
    
    }) 
</script>
@endsection