@extends('System.Layouts.Master')
@section('title')
DashBoard
@endsection
@section('css')
@endsection
@section('content-header')
<div class="container-fluid">
  <!-- Page-Title -->
  <div class="row">
    <div class="col-sm-12">
      <div class="page-title-box">

        <h4 class="page-title"> <i class="dripicons-meter"></i> Profile</h4>
      </div>
    </div>
  </div>
  <!-- end page title end breadcrumb -->


</div>
@endsection
@section('content-body')
<div class="container-fluid">

  <div class="row">
    <div class="col-md-12">
      <div class="col-md-6 col-xl-4 mx-auto">
        <div class="card text-center m-b-30">
          <div class="mb-2 card-body text-muted">
            <img src="assets/images/users/avatar-1.jpg" alt="">
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6 col-xl-3">
      <div class="card text-center m-b-30">
        <div class="mb-2 card-body text-muted">
          <h3 class="text-purple">321321321</h3>
          User ID
        </div>
      </div>
    </div>
    <div class="col-md-6 col-xl-6">
      <div class="card text-center m-b-30">
        <div class="mb-2 card-body text-muted">
          <h3 class="text-primary">2831213213@gmail.com9</h3>
          User Email
        </div>
      </div>
    </div>
    <div class="col-md-6 col-xl-3">
      <div class="card text-center m-b-30">
        <div class="mb-2 card-body text-muted">
          <h3 class="text-danger">5,220</h3>
          User Balance
        </div>
      </div>
    </div>
  </div>
  <!-- end row -->

  <div class="col-12">
    <div class="card m-b-20">
      <div class="card-title">Order History </div>
      <div class="card-body">
        <table id="datatable-buttons-5" class="table table-striped table-bordered dt-responsive nowrap" style="
          border-collapse: collapse;
          border-spacing: 0;
          width: 100%;
        ">
          <thead>
            <tr>
              <th>ID</th>
              <th>Product Order</th>
              <th>Product QTY Order</th>
              <th>Price Total</th>
              <th>Comment</th>
              <th>Email User</th>
              <th>ID User</th>
              <th>Date Create</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>

          <tbody>

            <tr>
              <td>Michelle House</td>
              <td>Integration Specialist</td>
              <td>Sidney</td>
              <td>Integration Specialist</td>
              <td>Integration Specialist</td>
              <td>Integration Specialist</td>
              <td>37</td>
              <td>2011/06/02</td>
              <td>
                <span class="badge badge-warning">
                  Waitting Ship</span>
                <span class="badge badge-danger">Is Cancel </span>
                <span class="badge badge-success">Success</span>

              </td>
              <td>

                <a class="btn btn-warning waves-effect  waves-light btn-sm" data-title="Detail bill 123">Detail Time
                  Ship</a>
                <!-- <button class="btn btn-danger waves-effect  waves-light btn-sm">Delete</button> -->

              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- end row -->

</div> <!-- end container -->
@endsection
@section('scripts')
<!-- Required datatable js -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/buttons.colVis.min.js"></script>
<!-- Responsive examples -->
<script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables/responsive.bootstrap4.min.js"></script>


<!-- Plugins js -->
<script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

<!-- Plugins Init js -->
<script src="assets/pages/form-advanced.js"></script>
<!-- Datatable init js -->
<script src="assets/pages/datatables.init.js"></script>

<!-- App js -->
<script src="assets/js/app.js"></script>
@endsection