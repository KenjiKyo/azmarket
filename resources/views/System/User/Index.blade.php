@extends('System.Layouts.Master')
@section('title')
DashBoard
@endsection
@section('css')


@endsection
@section('content-header')
<div class="container-fluid">
  <!-- Page-Title -->
  <div class="row">
    <div class="col-sm-12">
      <div class="page-title-box">

        <h4 class="page-title">
          <i class="dripicons-blog"></i> User Management
        </h4>
      </div>
    </div>
  </div>
  <!-- end page title end breadcrumb -->
</div>
@endsection
@section('content-body')
<div class="container-fluid">
  <div class="row">

    <div class="col-lg-8 mx-auto">
      <div class="card m-b-20">
        <div class="card-body">
          <h4 class="mt-0 header-title">Search</h4>
          <form action="#" id="form-suser">
            <div class="row">
              <div class="col-md-12 col-lg-6">
                <div class="form-group">
                  <label>User ID</label>
                  <input class="form-control" type="text" value="" id="example-text-input" placeholder="User ID">
                </div>
              </div>
              <div class="col-md-12 col-lg-6">
                <div class="form-group">
                  <label>User Email</label>
                  <input class="form-control" type="text" value="" id="example-text-input" placeholder="User Email">
                </div>
              </div>
              <div class="col-md-12 col-lg-6">
                <div class="form-group">
                  <label>User Level</label>
                  <select class="form-control">
                    <option>Select</option>
                    <option>Large select</option>
                    <option>Small select</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12 col-lg-6">
                <div class="form-group">
                  <label>User Status</label>
                  <select class="form-control">
                    <option>Select</option>
                    <option>Large select</option>
                    <option>Small select</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12 col-lg-6">
                <div class="form-group">
                  <label>Create From</label>
                  <div>
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                      <div class="input-group-append">
                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                      </div>
                    </div><!-- input-group -->
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-lg-6">
                <div class="form-group">
                  <label>Create To</label>
                  <div>
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose-1">
                      <div class="input-group-append">
                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                      </div>
                    </div><!-- input-group -->
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group text-center">
                  <button type="submit" class="btn btn-primary waves-effect  waves-light">Search</button>
                  <button type="button" onclick="resetForm()"
                    class="btn btn-warning  waves-effect waves-light">Cancel</button>
                    <button class="btn btn-success" type="button" data-toggle="modal" data-target="#modalEditCreate" data-action="create"
                        data-title="Create New User">Add New User</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>

    </div>
    <div class="col-12">
      <div class="card m-b-20">
        <div class="card-body">
          <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="
              border-collapse: collapse;
              border-spacing: 0;
              width: 100%;
            ">
            <thead>
              <tr>
                <th>User ID</th>
                <th>User Name</th>
                <th>User Email</th>
                <th>Date Create</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @foreach($UserList as $v)
              <tr>
                <td>{{ $v->user_id }}</td>
                <td>
                  {{ $v->user_name }}
                  @if($v->user_level == 1)
                  <span class="badge-danger badge">Admin</span>
                  @else
                  <span class="badge badge-success">Member</span>
                  @endif
                </td>
                <td>{{ $v->user_email }}</td>

                <td>{{ $v->created_at }}</td>
                <td>
                  @if($v->user_status == 1)
                  <span class="badge badge-success">Active</span>
                  @else
                  <span class="badge-danger badge">Not Active</span>
                  @endif
                  <!--
						<span class="badge badge-success">KYC Success!</span>
						
						<span class="badge badge-success">Auth</span>
						<span class="badge-danger badge">KYC Not very files</span>
						
						<span class="badge-danger badge">Not Auth</span>
-->
                </td>
                <td>
                  <!-- 	                  <button class="btn btn-warning waves-effect  waves-light btn-sm">Login</button> -->
                  <button class="btn btn-danger waves-effect  waves-light btn-sm block-button" data-userid="{{ $v->user_id }}">Block User
                    /unBlock</button>
                  <button class="btn btn-dark waves-effect  waves-light btn-sm block-reset" data-userid="{{ $v->user_id }}">Reset Password</button>
                  <button class="btn btn-warning waves-effect  waves-light btn-sm" data-toggle="modal" data-action="update"
                  data-target="#modalEditCreate" data-title="Edit Profile User: {{ $v->user_id }}" data-userid="{{ $v->user_id }}" data-username="{{ $v->user_name }}"
                  data-useremail="{{ $v->user_email }}" 
                >Edit</button>
                </td>
              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- end col -->
  </div>
  <!-- end row -->
</div>
<div class="modal fade bs-example-modal-lg" id="modalEditCreate" tabindex="-1" role="dialog"
  aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title mt-0"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">

        <form  method="POST" class="formCreate">
          @csrf
          <div class="row">
            <div class="col-md-12 col-lg-6">
              <div class="form-group">
                <label>Product Name</label>
                <input class="form-control" name="productName" type="text" value="" id="example-text-input" placeholder="User ID">
              </div>
            </div>
            <div class="col-md-12 col-lg-6">
              <div class="form-group">
                <label>Product QTY</label>
                <input class="form-control" name="productQuantity" type="number" value="" id="example-text-input" placeholder="User ID">
              </div>
            </div>
            <div class="col-md-12 col-lg-6">
              <div class="form-group">
                <label>Product Price</label>
                <input class="form-control" name="productPrice" type="number" value="" id="example-text-input" placeholder="User ID">
              </div>
            </div>
            
          <div class="col-md-12">
            <div class="form-group text-right">
              <button class="btn btn-success waves-light waves">Submit</button>
              <button class="btn btn-danger waves-light waves" data-dismiss="modal">Cancer</button>
            </div>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
@endsection
@section('scripts')
<!-- Required datatable js -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/buttons.colVis.min.js"></script>
<!-- Responsive examples -->
<script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables/responsive.bootstrap4.min.js"></script>


<!-- Plugins js -->
<script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

<!-- Plugins Init js -->
<script src="assets/pages/form-advanced.js"></script>
<!-- Datatable init js -->
<script src="assets/pages/datatables.init.js?v=1"></script>

<!-- App js -->
<script src="assets/js/app.js"></script>

<script>
    function resetForm(){
           document.getElementById("form-suser").reset();
       }
       
  </script>
<script>
  var token = '{{ csrf_token() }}';
  $(document).ready(function(){
      //block user alert
    $('#datatable-buttons').on('click','.block-button',function(){
        var userId = $(this).data('userid');
        
        console.log(userId);
        Swal.fire({
        title: 'Are you sure?',
        text: "Your action block this user !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, block this user!'
        }).then((result) => {
            if (result.value) {
              $.ajax({
                  type: 'POST',
                  url: "{{ route('system.updateUserStatus')}}" ,
                  data: {_token: token, user_id:userId},
                  dataType: 'JSON',
                  success: function (results) {
                      
                  let parsedData = JSON.parse(results);
                      
                      if (parsedData.status === true) {
                  
                          toastr.success("Done!", parsedData.message);
                          
                          setTimeout(function () {
                              location.reload();
                          },500)
                      } else {
                          toastr.error("Error", parsedData.message);
                      }
                  }
                });
               
            
               
            }
        })
    });
    //reset password user
    $('#datatable-buttons').on('click','.block-reset',function(){
        var userId = $(this).data('userid');
        Swal.fire({
        title: 'Are you sure?',
        text: "Your action reset password this user to default!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, reset password this user!'
        }).then((result) => {
            if (result.value) {
              $.ajax({
                  type: 'POST',
                  url: "{{ route('system.postChangePassword')}}" ,
                  data: {_token: token, user_id:userId},
                  dataType: 'JSON',
                  success: function (results) {
                      
                  let parsedData = JSON.parse(results);
                      
                      if (parsedData.status === true) {
                  
                          toastr.success("Done!", parsedData.message);
                          
                          setTimeout(function () {
                              location.reload();
                          },500)
                      } else {
                          toastr.error("Error", parsedData.message);
                      }
                  }
                });
          
               
            }
        })
    });
});

</script>
@endsection