<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>TDMT -  @yield('title') </title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <base href="{{ asset('/System').'/'}}">
        <!-- App Icons -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

     
      @include('System.Layouts.css')
      @yield('css')
    </head>


    <body>

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>
        <div class="header-bg">
          <!-- Navigation Bar-->
        @include('System.Layouts.Header')
        @yield('content-header')
        
      </div>

        <div class="wrapper">
          @yield('content-body')
     
        </div>
        <!-- end wrapper -->


        @include('System.Layouts.script')
        @yield('scripts')
        @include('shareAny.massageAlert')
        

    </body>
</html>