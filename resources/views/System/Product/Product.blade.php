@extends('System.Layouts.Master')
@section('title')
DashBoard
@endsection
@section('css')
@endsection
@section('content-header')
<div class="container-fluid">
  <!-- Page-Title -->
  <div class="row">
    <div class="col-sm-12">
      <div class="page-title-box">

        <h4 class="page-title">
          <i class="dripicons-blog"></i> Product Management
        </h4>
      </div>
    </div>
  </div>
  <!-- end page title end breadcrumb -->
</div>
@endsection
@section('content-body')
<div class="container-fluid">
  <div class="row">

    <div class="col-lg-8 mx-auto">
      <div class="card m-b-20">
        <div class="card-body">
          <div class="row">

            <div class="col-md-12">
              <h4 class="mt-0 header-title">Search</h4>
              <form action="#" id="form-suser">
                <div class="row">
                  <div class="col-md-12 col-lg-12">
                    <div class="form-group">
                      <label>Product Name</label>
                      <input class="form-control" type="text" value="" id="example-text-input" placeholder="User Email">
                    </div>
                  </div>
                  <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                      <label>Product ID</label>
                      <input class="form-control" type="text" value="" id="example-text-input" placeholder="User ID">
                    </div>
                  </div>

                  <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                      <label>Product Status</label>
                      <select class="form-control">
                        <option>Select</option>
                        <option>Large select</option>
                        <option>Small select</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                      <label>Create From</label>
                      <div>
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                          </div>
                        </div><!-- input-group -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                      <label>Create To</label>
                      <div>
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose-1">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                          </div>
                        </div><!-- input-group -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group text-center">
                      <button type="submit" class="btn btn-primary waves-effect  waves-light">Search</button>
                      <button type="button" onclick="resetForm()"
                        class="btn btn-warning  waves-effect waves-light">Cancel</button>
                      <button class="btn btn-success" type="button" data-toggle="modal" data-target="#modalEditCreate"
                        data-action="create" data-title="Create New Products">Add New Products</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>


        </div>
      </div>

    </div>
    <div class="col-12">
      <div class="card m-b-20">
        <div class="card-body">
          <table id="datatable-buttons-3" class="table table-striped table-bordered dt-responsive " style="
              border-collapse: collapse;
              border-spacing: 0;
              width: 100%;
            ">
            <thead>
              <tr>

                <th>ID</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Price Max</th>
                <th>Price Min</th>
                <th>Vote</th>
                <th>Image</th>
                <th>Category</th>
                <th>Description</th>
                <th>Content</th>
                <th>Title</th>
                {{-- <th>Status</th> --}}
                <th>Date Create</th>
                <th>Date Update</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @foreach($product as $tt)
              <tr>
                <td>{{ $tt->_id }}</td>
                <td>{{ $tt->product_name }}</td>
                <td>{{ $tt->product_quantity }}</td>
                <td>{{ $tt->product_price}}</td>
                <td>{{ $tt->product_priceMax}}</td>
                <td>{{ $tt->product_priceMin}}</td>
                <td>{{ $tt->product_vote}}</td>
                <td>{{ $tt->product_minimumPeople}}</td>
                <td text-center><img class="img-thumbnail " style="cursor: pointer;height:150px; " data-toggle="modal"
                    data-target="#detailImage" alt="" width="150px"
                    src="https://mediatmdt.chidetest.com/{{ $tt->product_images }} " data-holder-rendered="true"
                    data-image="assets/images/small/img-3.jpg"></td>
                <td>{{ $tt->product_categoryId }}</td>
                <td>{{ $tt->product_description }}</td>
                <td>{{ $tt->product_content}}</td>
                <td>{{ $tt->product_title}}</td>
                {{-- <td>{{ $tt->product_status}}</td> --}}
                <td>{{ $tt->updated_at }}</td>
                {{-- <td>{{ $tt->created_at }}</td> --}}
                <td>
                  <span class="badge badge-warning">
                    almost over</span>
                  <span class="badge badge-danger">over</span>

                </td>
                <td>
                  <button class="btn btn-warning waves-effect  waves-light btn-sm" data-toggle="modal"
                    data-action="update" data-target="#modalEditCreate" data-title="Edit Product 123"
                    data-product_id="{{ $tt->_id }}" data-name="{{ $tt->product_name }}"
                    data-price="{{ $tt->product_price }}" data-qty="{{ $tt->product_quantity }}"
                    data-priceMin="{{ $tt->product_priceMin }}" data-priceMax="{{ $tt->product_priceMax }}"
                    data-vote="{{ $tt->product_vote }}" data-minimumPeople="{{ $tt->product_minimumPeople }}"
                    data-title="{{ $tt->product_title }}" data-type="{{ $tt->product_categoryId }}"
                    data-description="{{$tt->product_description }}" data-img="{{ $tt->product_images }}">Edit</button>
                  <button class="btn btn-danger waves-effect  waves-light btn-sm button-delete"
                    data-product_id="{{ $tt->_id }}">Delete</button>

                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- end col -->
  </div>
  <!-- end row -->
</div>
<div class="modal fade bs-example-modal-lg" id="modalEditCreate" tabindex="-1" role="dialog"
  aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title mt-0"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        {{-- @if(count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $err)
                {{ $err }}<br>
        @endforeach
      </div>
      @endif
      @if(session('thongbao'))
      <div class="alert alert-success">
        {{ session('thongbao') }}
      </div>
      @endif --}}
      <form method="POST" class="formCreate" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-md-12 col-lg-6 edit-id d-none">
            <div class="form-group">
              <label>Product ID</label>
              <input class="form-control productid" name="product_id" type="text " value="" id="example-text-input"
                placeholder="User ID">
            </div>
          </div>
          <div class="col-md-12 col-lg-6 ">

            <div class="form-group">
              <label>Product Name</label>
              <input class="form-control productname" name="productName" type="text " value="" id="example-text-input"
                placeholder="User ID">
            </div>
          </div>
          <div class="col-md-12 col-lg-6">
            <div class="form-group">
              <label>Product QTY</label>
              <input class="form-control productQuantity" name="productQuantity" type="number" value=""
                id="example-text-input" placeholder="User ID">
            </div>
          </div>
          <div class="col-md-12 col-lg-6">
            <div class="form-group">
              <label>Product Price</label>
              <input class="form-control productPrice" name="productPrice" type="number" value=""
                id="example-text-input" placeholder="User ID">
            </div>
          </div>
          <div class="col-md-12 col-lg-6">
            <div class="form-group">
              <label>Product Price Min</label>
              <input class="form-control productPriceMin" name="productPriceMin" type="number" value=""
                id="example-text-input" placeholder="User ID">
            </div>
          </div>
          <div class="col-md-12 col-lg-6">
            <div class="form-group">
              <label>Product Price Max</label>
              <input class="form-control productPriceMax" name="productPriceMax" type="number" value=""
                id="example-text-input" placeholder="User ID">
            </div>
          </div>
          <div class="col-md-12 col-lg-6">
            <div class="form-group">
              <label>Vote</label>
              <input class="form-control productVote" name="productVote" type="number" value="" id="example-text-input"
                placeholder="User ID">
            </div>
          </div>
          <div class="col-md-12 col-lg-6">
            <div class="form-group">
              <label>Min People</label>
              <input class="form-control productMinPeople" name="productMinPeople" type="number" value=""
                id="example-text-input" placeholder="User ID">
            </div>
          </div>
          <div class="col-md-12 col-lg-6">
            <div class="form-group">
              <label>Title</label>
              <input class="form-control productTitle" name="productTitle" type="text" value="" id="example-text-input"
                placeholder="User ID">
            </div>
          </div>
          {{-- <div class="col-md-12 col-lg-6">
                <div class="form-group">
                  <label>Status</label>
                  <input class="form-control" name="productStatus" type="number" value="1" id="example-text-input" placeholder="User ID">
                </div>
              </div> --}}
          <div class="col-md-12 col-lg-6">
            <div class="form-group">
              <label>Product Type</label>
              <select name="productType" class="select2 form-control select2-multiple" multiple="multiple" multiple
                data-placeholder="Choose ...">
                <optgroup label="Alaskan/Hawaiian Time Zone">
                  <option value="AK">Alaska</option>
                  <option value="HI">Hawaii</option>
                </optgroup>
                <optgroup label="Pacific Time Zone">
                  <option value="idtyproduct">name type product</option>
                  <option value="NV">Nevada</option>
                  <option value="OR">Oregon</option>
                  <option value="WA">Washington</option>
                </optgroup>

              </select>

            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>description</label>
              <textarea id="elm1" name="productDescription" class="form-control productDescription"></textarea>
            </div>
          </div>
          <div class="col-md-12 col-lg-6 img-detail d-none">
            <div class="form-group">

              <img class="productImages" alt="" width="200px" height="200px">
            </div>
          </div>
          <div class="form-group col-lg-6">
            <div class="form-group">
              <label> set new Image Product</label>
              <input type="file" name="productImages" class="filestyle" data-buttonname="btn-secondary">
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group text-right">
            <button class="btn btn-success waves-light waves">Submit</button>
            <button class="btn btn-danger waves-light waves" data-dismiss="modal">Cancer</button>
          </div>
        </div>
      </form>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
<div class="modal fade bs-example-modal-center" id="detailImage" tabindex="-1" role="dialog"
  aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title mt-0"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <img class="img-thumbnail" alt="200x200" width="100%" src="assets/images/small/img-3.jpg">
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
@endsection
@section('scripts')
<!-- Required datatable js -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/buttons.colVis.min.js"></script>
<!-- Responsive examples -->
<script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables/responsive.bootstrap4.min.js"></script>


<!-- Plugins js -->
<script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

<!-- Plugins Init js -->
<script src="assets/pages/form-advanced.js"></script>
<!-- Datatable init js -->
<script src="assets/pages/datatables.init.js?v=1"></script>

<!-- App js -->
<script src="assets/js/app.js"></script>
<script src="assets/plugins/tinymce/tinymce.min.js"></script>

<script>
    var token = '{{ csrf_token() }}';
  $(document).ready(function () {
      if($("#elm1").length > 0){
          tinymce.init({
              selector: "textarea#elm1",
              theme: "modern",
              height:300,
              plugins: [
                  "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                  "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                  "save table contextmenu directionality emoticons template paste textcolor"
              ],
              toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
              style_formats: [
                  {title: 'Bold text', inline: 'b'},
                  {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                  {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                  {title: 'Example 1', inline: 'span', classes: 'example1'},
                  {title: 'Example 2', inline: 'span', classes: 'example2'},
                  {title: 'Table styles'},
                  {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
              ]
          });
      }
  });
</script>
<script>
  function resetForm(){
         document.getElementById("form-suser").reset();
     }
     $('#modalEditCreate').on('show.bs.modal', function (event) {
       var button = $(event.relatedTarget)
      

    

       var title = button.data('title')
       var action = button.data('action')
       var modal = $(this)
       modal.find('.modal-title').text(title)
       if(action == 'create')
       {
         $('.formCreate').attr('action', `{{ route('system.postAddProduct') }}`)
         $('.img-detail').addClass('d-none')
         $('.edit-id').addClass('d-none')
       }
       else{
         $('.formCreate').attr('action',`{{ route('system.postEditProduct')}}`)
        $('.img-detail').removeClass('d-none')
        $('.img-detail').addClass('d-block')
        $('.edit-id').removeClass('d-none')
        $('.edit-id').addClass('d-block')
        var name = button.data('name')
        var price = button.data('price')
        var quantity = button.data('qty')
        var priceMin = button.data('pricemin')
        var priceMax = button.data('pricemax')
        var Vote = button.data('vote')
        var MinPeople = button.data('minimumpeople')
        var title = button.data('title')
        var type = button.data('type')
        var description = button.data('description')
        var img = button.data('img')
        var product_id= button.data('product_id')
          console.log(product_id);
        modal.find('.productname').val(name)
        modal.find('.productPrice').val(price)
        modal.find('.productQuantity').val(quantity)
        modal.find('.productPriceMin').val(priceMin)
        modal.find('.productPriceMax').val(priceMax)
        modal.find('.productVote').val(Vote)
        modal.find('.productMinPeople').val(MinPeople)
        modal.find('.productTitle').val(title)
        modal.find('.productType').val(type)
        modal.find('.productid').val(product_id)
        // tinymce.get("#elm1").setContent(description);
        tinyMCE.activeEditor.setContent(description, {format : 'raw'});
        modal.find('.productImages').val(img)
        modal.find('.productImages').attr('src','https://mediatmdt.chidetest.com/'+img)
       }


     })

     $('#modalEditCreate').on('click', '.btn-sm',function () {

     })
</script>
<!--Wysiwig js-->

<script>
  $(document).ready(function(){
      //block user alert
    $('#datatable-buttons-3').on('click','.button-delete',function(){
        var product_id = $(this).data('product_id');
        Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, deleit!'
        }).then((result) => {
           if (result.value) {
             $.ajax({
              type: 'POST',
                url: "{{ route('system.postDeleteProduct')}}" ,
                data: {_token: token, product_id:product_id},
                dataType: 'JSON',
                success: function (results) {
                  console.log(results);
                    if (results.success === true) {
                       
                        swal("Done!", results.message, "success");
                    } else {
                        swal("Error!", results.message, "error");
                    }
                }
             })
               Swal.fire(
               'Deleted!',
               'Your file has been deleted.',
               'success'
               )
           }
       })
    });
  });
</script>
@endsection