<?php


return [
    'title' => 'TMDT',
    'icon' => '',
    'logo' => '',
    'bg-form' => '',
    'bg-cover' => '',
    'style-image' => 'width: 100%;',
    'description' => '',
    'name_auth' => 'TMDT',
    'url_upload' => 'https://mediatmdt.chidetest.com/',
    'url_web' => 'https://tmdt.chidetest.com'
];
