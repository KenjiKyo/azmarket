/*
 Template Name: Fonik - Responsive Bootstrap 4 Admin Dashboard
 Author: Themesbrand
 File: Datatable js
 */

$(document).ready(function() {
    $('#datatable').DataTable();

    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        searching: false,
        "columnDefs": [
            {  // set default column settings
                "orderable": false,
                "targets": [1, 2, 4]
            }
        ]
    });
    //Buttons examples
    var table = $('#datatable-buttons-2').DataTable({
        lengthChange: false,
        searching: false,
        "columnDefs": [
            {  // set default column settings
                "orderable": false,
                "targets": [1, 2, 4]
            }
        ]
    });

    //Buttons examples
        var table = $('#datatable-buttons-3').DataTable({
            lengthChange: false,
            searching: false,
            "columnDefs": [
                {  // set default column settings
                    "orderable": false,
                    "targets": [1, 2, 4]
                }
            ]
        });
        //Buttons examples
        var table = $('#datatable-buttons-4').DataTable({
            lengthChange: false,
            searching: false,
            "columnDefs": [
                {  // set default column settings
                    "orderable": false,
                    "targets": [1, 2, 4]
                }
            ]
        });
    
    
    table.buttons().container()
        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
} );